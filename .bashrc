export PATH="$HOME/Scripts":$PATH
export PATH="$HOME/.emacs.d/bin":$PATH
export LC_ALL=en_US.utf8

export VISUAL=neovide
export EDITOR="$VISUAL"
export TERMINAL="st"

# FFF ENV VARS
export FFF_HIDDEN=1

alias be="$EDITOR ~/.bashrc"
alias bs="source ~/.bashrc"
alias ve="$EDITOR ~/.vimrc"
alias test-mic="arecord -vvv -f dat /dev/null"
alias pqs="pacman -Ss"
alias paci="doas pacman -S"
alias yqs="yay -Ss"
alias yayi="yay -S"
alias yt="ytfzf -flt"
alias ytm="ytfzf -ltm"
alias v="vim"
alias edit="$EDITOR"
alias e="edit"
alias lf="lfrun"
alias ls='ls --color=auto'

[[ $- != *i* ]] && return

# PS1='[\u@\h \W]\$ '
PS1="\[\e[31m\][\[\e[33m\]\u\[\e[32m\]@\[\e[34m\]\h \[\e[37m\]\w\[\e[31m\]]\[\e[37m\]$ "
#LMAO COMMENT
